package me.randomhashtags.fatbucket.utils;

import me.randomhashtags.fatbucket.FatBucket;
import org.bukkit.ChatColor;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.HashMap;
import java.util.List;
import java.util.UUID;

public class ProgressBucket {
    private static final FatBucket fb = FatBucket.getPlugin;
    public static final HashMap<UUID, ProgressBucket> buckets = new HashMap<>();
    public final CustomBucket bucket;
    public final UUID uuid;
    public final ItemStack is;
    public int progress;
    public ProgressBucket(ItemStack is, CustomBucket bucket, UUID uuid) {
        final ProgressBucket pb = valueOf(is);
        if(pb != null) {
            this.progress = pb.progress;
            this.bucket = pb.bucket;
            this.uuid = pb.uuid;
            this.is = pb.is;
        } else {
            this.progress = 0;
            this.bucket = bucket;
            this.uuid = uuid;
            this.is = is;
            buckets.put(uuid, this);
        }
    }
    public static ProgressBucket valueOf(ItemStack is) {
        if(is != null && is.hasItemMeta() && is.getItemMeta().hasDisplayName() && is.getItemMeta().hasLore()) {
            final ItemMeta im = is.getItemMeta();
            final List<String> l = im.getLore();
            final String n = im.getDisplayName();
            try {
                final int s = l.size(), percent = fb.getRemainingInt(im.getDisplayName());
                final UUID u = UUID.fromString(ChatColor.stripColor(l.get(s-1)));
                for(CustomBucket b : CustomBucket.buckets) {
                    final ItemStack p = b.getPercent(percent, u, false);
                    final ItemMeta itemmeta = p.getItemMeta();
                    if(n.equals(itemmeta.getDisplayName()) && l.equals(itemmeta.getLore())) {
                        return buckets.get(u);
                    }
                }
            } catch (Exception e) {
                // only for the UUID error
            }
        }
        return null;
    }
}