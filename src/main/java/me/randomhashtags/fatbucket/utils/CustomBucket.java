package me.randomhashtags.fatbucket.utils;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;

public class CustomBucket {
    public static final List<CustomBucket> buckets = new ArrayList<>();
    public final String path, name, statusPercent, statusUses;
    public final int uses, sourcesRequired;
    public final List<String> lore, fillableWorlds, usableWorlds, filledMsg;
    public CustomBucket(String path, int uses, int sourcesRequired, String name, List<String> lore, String statusPercent, String statusUses, List<String> fillableWorlds, List<String> usableWorlds, List<String> filledMsg) {
        this.path = path;
        this.uses = uses;
        this.sourcesRequired = sourcesRequired;
        this.name = ChatColor.translateAlternateColorCodes('&', name);
        final List<String> L = new ArrayList<>();
        for(String s : lore) L.add(ChatColor.translateAlternateColorCodes('&', s));
        this.lore = L;
        this.statusPercent = ChatColor.translateAlternateColorCodes('&', statusPercent);
        this.statusUses = ChatColor.translateAlternateColorCodes('&', statusUses);
        this.fillableWorlds = fillableWorlds;
        this.usableWorlds = usableWorlds;
        this.filledMsg = filledMsg;
        buckets.add(this);
    }
    public ItemStack getDefault() {
        final ItemStack i = new ItemStack(Material.BUCKET);
        final ItemMeta im = i.getItemMeta();
        im.setDisplayName(name + statusPercent.replace("{PERCENT}", "0"));
        final List<String> A = new ArrayList<>(lore);
        final UUID u = UUID.randomUUID();
        A.add(ChatColor.BLACK + u.toString());
        im.setLore(A);
        i.setItemMeta(im);
        new ProgressBucket(i, this, u);
        return i;
    }
    public ItemStack getProgress(int progress, UUID uuid, boolean createNew) {
        final int p = (int) ((((double) progress)/(double)sourcesRequired)*100);
        return getPercent(p, uuid, createNew);
    }
    public ItemStack getPercent(int p, UUID uuid, boolean createNew) {
        final HashMap<UUID, ProgressBucket> buck = ProgressBucket.buckets;
        final ProgressBucket pb = buck.keySet().contains(uuid) ? buck.get(uuid) : null;
        if(p >= 100) {
            if(pb != null) buck.remove(uuid);
            return getUses(uses);
        }
        final ItemStack i = new ItemStack(Material.BUCKET);
        final ItemMeta im = i.getItemMeta();
        im.setDisplayName(name + statusPercent.replace("{PERCENT}", Integer.toString(p)));
        final List<String> A = new ArrayList<>(lore);
        A.add(ChatColor.BLACK + uuid.toString());
        im.setLore(A);
        i.setItemMeta(im);
        if(createNew && pb == null) new ProgressBucket(i, this, uuid);
        return i;
    }
    public ItemStack getUses(int u) {
        if(u <= 0) return getPercent(0, UUID.randomUUID(), true);
        final ItemStack i = new ItemStack(Material.LAVA_BUCKET);
        final ItemMeta im = i.getItemMeta();
        im.setDisplayName(name + statusUses.replace("{USES}", Integer.toString(u)));
        im.setLore(lore);
        i.setItemMeta(im);
        return i;
    }
    public static CustomBucket valueOf(String path) {
        for(CustomBucket b : buckets)
            if(b.path.equals(path))
                return b;
        return null;
    }
    public static CustomBucket valueOfUses(ItemStack i) {
        if(i != null && i.getType().equals(Material.LAVA_BUCKET) && i.hasItemMeta() && i.getItemMeta().hasDisplayName() && i.getItemMeta().hasLore()) {
            final ItemMeta im = i.getItemMeta();
            final String n = im.getDisplayName(), number = Integer.toString(getRemainingInt(n));
            final List<String> l = im.getLore();
            for(CustomBucket b : buckets)
                if(b.lore.equals(l) && n.endsWith(b.statusUses.replace("{USES}", number)))
                    return b;
        }
        return null;
    }
    private static int getRemainingInt(String string) {
        string = ChatColor.stripColor(ChatColor.translateAlternateColorCodes('&', string)).replaceAll("\\p{L}", "").replaceAll("\\s", "").replaceAll("\\p{P}", "").replaceAll("\\p{S}", "");
        return string.equals("") ? -1 : Integer.parseInt(string);
    }
}
