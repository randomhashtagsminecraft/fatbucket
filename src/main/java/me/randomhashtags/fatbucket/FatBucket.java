package me.randomhashtags.fatbucket;

import me.randomhashtags.fatbucket.utils.CustomBucket;
import me.randomhashtags.fatbucket.utils.ProgressBucket;
import org.bukkit.*;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Item;
import org.bukkit.entity.Player;
import org.bukkit.event.*;
import org.bukkit.event.entity.EntityCombustEvent;
import org.bukkit.event.entity.ItemDespawnEvent;
import org.bukkit.event.player.PlayerBucketEmptyEvent;
import org.bukkit.event.player.PlayerBucketFillEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.File;
import java.util.HashMap;
import java.util.UUID;

public final class FatBucket extends JavaPlugin implements Listener {

    public static FatBucket getPlugin;
    public final HashMap<Player, ItemStack> using = new HashMap<>();
    public final HashMap<Player, ProgressBucket> progressBuckets = new HashMap<>();
    public final HashMap<Player, CustomBucket> usesBuckets = new HashMap<>();
    private PluginManager pm;
    private final String v = Bukkit.getVersion();

    private File dataF;
    private YamlConfiguration data;

    @Override
    public void onEnable() {
        getPlugin = this;
        saveDefaultConfig();
        for(String s : getConfig().getConfigurationSection("fat buckets").getKeys(false)) {
            final String p = "fat buckets." + s + ".";
            new CustomBucket(s, getConfig().getInt(p + "uses"), getConfig().getInt(p + "sources required"), getConfig().getString(p + "name"), getConfig().getStringList(p + "lore"), getConfig().getString(p + "percent full status"), getConfig().getString(p + "uses left status"), getConfig().getStringList(p + "fillable in worlds"), getConfig().getStringList(p + "usable in worlds"), getConfig().getStringList(p + ".only filled in world message"));
        }
        pm = Bukkit.getPluginManager();
        pm.registerEvents(this, this);
        save(null, "_data.yml");
        dataF = new File(getDataFolder() + File.separator, "_data.yml");
        data = YamlConfiguration.loadConfiguration(dataF);

        loadBackup();
    }

    @Override
    public void onDisable() {
        backup();
    }

    private void save(String folder, String file) {
        File f = null;
        final File d = getDataFolder();
        if(folder != null && !folder.equals(""))
            f = new File(d + File.separator + folder + File.separator, file);
        else
            f = new File(d + File.separator, file);
        if(!f.exists()) {
            f.getParentFile().mkdirs();
            saveResource(folder != null && !folder.equals("") ? folder + File.separator + file : file, false);
        }
    }

    public void reload() {
        backup();
        loadBackup();
    }

    private void backup() {
        final HashMap<UUID, ProgressBucket> pb = ProgressBucket.buckets;
        data.set("fat buckets", null);
        for(UUID u : pb.keySet()) {
            final ProgressBucket p = pb.get(u);
            final String path = "fat buckets." + u.toString() + ".";
            data.set(path + "type", p.bucket.path);
            data.set(path + "progress", p.progress);
        }
        save();
    }
    private void save() {
        try {
            data.save(dataF);
            dataF = new File(getDataFolder() + File.separator, "_data.yml");
            data = YamlConfiguration.loadConfiguration(dataF);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    private void loadBackup() {
        final ConfigurationSection section = data.getConfigurationSection("fat buckets");
        if(section != null) {
            for(String s : section.getKeys(false)) {
                final String Q = "fat buckets." + s + ".";
                final UUID uuid = UUID.fromString(s);
                final int progress = data.getInt(Q + "progress");
                final CustomBucket cb = CustomBucket.valueOf(data.getString(Q + "type"));
                final ItemStack it = cb.getProgress(progress, uuid, false);
                final ProgressBucket pb = new ProgressBucket(it, cb, uuid);
                pb.progress = progress;
            }
        }
    }

    @EventHandler
    private void itemDespawnEvent(ItemDespawnEvent event) {
        if(!event.isCancelled()) {
            final ItemStack i = event.getEntity().getItemStack();
            if(i != null && i.hasItemMeta() && i.getItemMeta().hasDisplayName() && i.getItemMeta().hasLore()) {
                final ProgressBucket p = ProgressBucket.valueOf(i);
                if(p != null) ProgressBucket.buckets.remove(p.uuid);
            }
        }
    }
    @EventHandler
    private void entityCombustEvent(EntityCombustEvent event) {
        if(!event.isCancelled()) {
            final ItemStack i = event.getEntity() instanceof Item ? ((Item) event.getEntity()).getItemStack() : null;
            if(i != null && i.hasItemMeta() && i.getItemMeta().hasDisplayName() && i.getItemMeta().hasLore()) {
                final ProgressBucket p = ProgressBucket.valueOf(i);
                if(p != null) ProgressBucket.buckets.remove(p.uuid);
            }
        }
    }
    @EventHandler
    private void playerBucketFillEvent(PlayerBucketFillEvent event) {
        final Player player = event.getPlayer();
        final Block clicked = event.getBlockClicked();
        if(progressBuckets.keySet().contains(player)) {
            event.setCancelled(true);
            final ProgressBucket pb = progressBuckets.get(player);
            final CustomBucket b = pb.bucket;
            if(clicked.getType().name().contains("WATER")) {
                clicked.setType(Material.WATER);
            } else {
                if(b.fillableWorlds.contains(clicked.getWorld().getName())) {
                    clicked.setType(Material.AIR);
                    pb.progress += 1;
                    final double p = (((double) pb.progress)/(b.sourcesRequired))*100;
                    player.setItemInHand(b.getPercent((int) p, pb.uuid, true));
                } else {
                    clicked.setType(Material.LAVA);
                    for(String s : b.filledMsg) player.sendMessage(ChatColor.translateAlternateColorCodes('&', s));
                }
            }
            player.updateInventory();
            using.remove(player);
            progressBuckets.remove(player);
        }
    }
    @EventHandler
    private void playerInteractEvent(PlayerInteractEvent event) {
        final Player player = event.getPlayer();
        final ItemStack i = event.getItem();
        final CustomBucket c = CustomBucket.valueOfUses(i);
        if(c != null) {
            using.put(player, i);
            usesBuckets.put(player, c);
        }
        final ProgressBucket b = ProgressBucket.valueOf(i);
        if(b != null) {
            using.put(player, i);
            progressBuckets.put(player, b);
        }
    }
    @EventHandler
    private void playerBucketEmptyEvent(PlayerBucketEmptyEvent event) {
        final Player player = event.getPlayer();
        if(usesBuckets.keySet().contains(player)) {
            event.setCancelled(true);
            final ItemStack i = using.get(player);
            final CustomBucket b = usesBuckets.get(player);
            final Location l = event.getBlockClicked().getLocation();
            final World w = l.getWorld();
            if(b.usableWorlds.contains(w.getName())) {
                player.setItemInHand(b.getUses(getRemainingInt(i.getItemMeta().getDisplayName())-1));
                player.updateInventory();
                usesBuckets.remove(player);
                using.remove(player);
                w.getBlockAt(getPlacedLocation(l, event.getBlockFace())).setType(Material.LAVA);
            }
        }
    }
    private Location getPlacedLocation(Location l, BlockFace b) {
        final int x = b == BlockFace.WEST ? -1 : b == BlockFace.EAST ? 1 : 0, y = b == BlockFace.DOWN ? -1 : b == BlockFace.UP ? 1 : 0, z = b == BlockFace.SOUTH ? 1 : b == BlockFace.NORTH ? -1 : 0;
        return new Location(l.getWorld(), l.getBlockX()+x, l.getBlockY()+y, l.getBlockZ()+z);
    }
    public int getRemainingInt(String string) {
        string = ChatColor.stripColor(ChatColor.translateAlternateColorCodes('&', string)).replaceAll("\\p{L}", "").replaceAll("\\s", "").replaceAll("\\p{P}", "").replaceAll("\\p{S}", "");
        return string.equals("") ? -1 : Integer.parseInt(string);
    }
}
